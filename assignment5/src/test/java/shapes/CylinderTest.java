/**
 * @author Sila Ben Khelifa
 */

package shapes;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {
    private final double TOLERANCE = 0.00001;
   
    @Test
    public void testCylinder() {
        Cylinder c = new Cylinder(2, 3);
        assertEquals(c.getRadius(), 2, TOLERANCE);
        assertEquals(c.getHeight(), 3, TOLERANCE);
    }

    @Test 
    public void testGetVolume() {
        Cylinder c = new Cylinder(2, 3);
        assertEquals(37.699112, c.getVolume(), TOLERANCE);
    }

    @Test
    public void testGetSurfaceArea() {
        Cylinder c = new Cylinder(2, 3);
        assertEquals(62.83185, c.getSurfaceArea(), TOLERANCE);
    }

}
