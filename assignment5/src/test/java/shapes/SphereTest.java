/**
 * @author 
 * Pera Nicholls
 * 2145293
 */
package shapes;
import org.junit.Test;
import static org.junit.Assert.*;

public class SphereTest {

    private final double TOLERANCE=0.0001;

    @Test
    public void testSphere() {
        Sphere orb = new Sphere(3);
        assertEquals(3, orb.getRadius(), TOLERANCE);
    }

    @Test
    public void testVolume(){
        Sphere orb= new Sphere (3);
        assertEquals(113.0973, orb.getVolume(), TOLERANCE);
    }

    @Test
    public void testSurfaceArea(){
        Sphere orb= new Sphere (4);
        assertEquals(orb.getSurfaceArea(), 201.0619, TOLERANCE);
    }
}
