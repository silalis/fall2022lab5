/**
 * @author
 * Pera Nicholls
 * 2145293
 */

package shapes;
public class Cylinder implements Shape3d {

    private final double PI=Math.PI;
    private double radius;
    private double height;

    /**
     * @param radius
     * @param height
     */
    public Cylinder(double radius, double height){
        this.radius=radius;
        this.height=height;
    }

    /**
     * @return double height
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * @return double radius
     */
    public double getRadius() {
        return this.radius;
    }


    /**
     * @return double volume
     */
    public double getVolume(){
        double circleArea=PI*Math.pow(this.radius, 2);
        return circleArea*this.height;
    }
    
    /**
     * @return double surfaceArea
     */
    public double getSurfaceArea(){
        double circleArea=PI*Math.pow(this.radius, 2);
        return (2*circleArea)+(2*PI*this.radius*this.height);
    }
}
