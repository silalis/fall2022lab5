/**
 * @author Sila Ben Khelifa
 * @date 9/30/2022
 */

package shapes;

public class Sphere implements Shape3d {
    private final double PI = Math.PI;
    private double radius;
    
    /**
     * @param radius
     */
    public Sphere(double radius){
       this.radius = radius;
    }

    /**
     * @return double radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * Method to return the volume of a sphere object
     * @return double volume
     */
    public double getVolume() {
       return (4.0/3.0)*this.PI*Math.pow(this.radius, 3);
    }

    /**
     * Method to return the surface area of a sphere object
     * @return double surfaceArea
     */
    public double getSurfaceArea() {
        return 4*this.PI*this.radius*this.radius;
    }
}
